<?php
  $host = 'localhost';
  $db = 'global';
  $usr = 'root';
  $pass = '';
  $charset = 'utf8';

  $pdo = new PDO("mysql:host={$host};dbname={$db};charset={$charset}", $usr, $pass);

  $sql = "SELECT * FROM books";

  if (!empty($_POST)) {
    $sql = "SELECT * FROM books WHERE 
      name LIKE '%{$_POST['name']}%' AND 
      isbn LIKE '%{$_POST['isbn']}%' AND 
      author LIKE '%{$_POST['author']}%'";
  }
?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="main.css">
</head>
<body>
  <div class="wrap">
    <h2 class="title">Библиотека успешного человека</h2>
    <form action="index.php" method="POST">
      <label for="name">Name</label>
      <input type="text" name="name" id="name" value="<?php echo (!empty($_POST['name'])) ? $_POST['name'] : '' ?>" autocomplete="off">
      <label for="isbn">ISBN</label>
      <input type="text" name="isbn" id="isbn" value="<?php echo (!empty($_POST['isbn'])) ? $_POST['isbn'] : '' ?>" autocomplete="off">
      <label for="author">Author</label>
      <input type="text" name="author" id="author" value="<?php echo (!empty($_POST['author'])) ? $_POST['author'] : '' ?>" autocomplete="off">
      <input type="submit" value="Применить фильтр">
    </form>
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Author</th>
          <th>Year</th>
          <th>ISBN</th>
          <th>Genre</th>
        </tr>
      </thead>
    <?php foreach($pdo->query($sql) as $row): ?>
      <tr>
        <td><?php echo $row['name'] ?></td>
        <td><?php echo $row['author'] ?></td>
        <td><?php echo $row['year'] ?></td>
        <td><?php echo $row['isbn'] ?></td>
        <td><?php echo $row['genre'] ?></td>
      </tr>
    <?php endforeach; ?>
    </table>
  </div>
</body>
</html>